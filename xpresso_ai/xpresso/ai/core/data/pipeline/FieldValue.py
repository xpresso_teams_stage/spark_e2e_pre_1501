from enum import  Enum


class FieldValue(Enum):
    """
    Enum class to standardize all the database collection values(predefined)
    for certain fields
    """

    RUN_STATUS_RUNNING = "RUNNING"
    RUN_STATUS_IDLE = "IDLE"
    RUN_STATUS_RESTART = "RESTARTED"
    RUN_STATUS_PAUSED = "PAUSED"
    RUN_STATUS_TERMINATE = "TERMINATE"
    RUN_STATUS_COMPLETED="COMPLETED"

    def __str__(self):
        return self.value
